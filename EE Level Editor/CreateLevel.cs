﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EE_Level_Editor
{
    public partial class CreateLevel : Form
    {
        public CreateLevel()
        {
            InitializeComponent();
        }

        private void okButton_Click(object sender, EventArgs e)
        {
            if (nameLvl.Text.Equals(""))
            {
                MessageBox.Show("Отсутствует имя уровня", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if ((widthLvl.Text.Equals("")) || (heightLvl.Text.Equals("")) ||
                (int.Parse(widthLvl.Text) == 0) || (int.Parse(heightLvl.Text) == 0))
            {
                MessageBox.Show("Неверно введено разрешение для уровня", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                ((EELevelEditorForm)this.Owner).create(nameLvl.Text, int.Parse(widthLvl.Text), int.Parse(heightLvl.Text), this.button3.BackColor);
                this.Close();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (colorDialog1.ShowDialog().ToString() == "OK")
            {
                button3.BackColor = colorDialog1.Color;
            }
        }

        private void resolutionLvl_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar <= 47 || e.KeyChar >= 58) && e.KeyChar != 8)
                e.Handled = true;
        }

        private void heightLvl_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
