﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EE_Level_Editor
{
    public partial class DigitalInputForm : Form
    {
        public DigitalInputForm(string caption)
        {
            InitializeComponent();
            this.Text = caption;
        }

        public int digitalValue
        {
            get { return int.Parse(digitalBox.Text); }
        }


        private void okButton_Click(object sender, EventArgs e)
        {
            if (digitalBox.Text.Equals(""))
            {
                MessageBox.Show("Не указано количество блоков", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                DialogResult = DialogResult.None;
            }
        }

        private void digitalBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar <= 47 || e.KeyChar >= 58) && e.KeyChar != 8)
                e.Handled = true;
        }
    }
}
