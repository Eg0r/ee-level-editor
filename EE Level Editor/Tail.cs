﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Drawing;

namespace EE_Level_Editor
{
    class Tail
    {
        private Bitmap bmp;
        private Point pXY;
        private string pathOfTail;
        private bool z = false;

        public Tail(string path, int x, int y)
        {
            bmp = new Bitmap(path);
            pXY = new Point(x, y);
            pathOfTail = path;
        }

        public Tail(string path, int x, int y, bool zIndex)
        {
            bmp = new Bitmap(path);
            pXY = new Point(x, y);
            pathOfTail = path;
            z = zIndex;
        }

        public Bitmap picture
        {
            get { return bmp; }
        }

        public int X
        {
            get { return pXY.X; }
            set { pXY.X = value; }
        }

        public int Y
        {
            get { return pXY.Y; }
            set { pXY.Y = value; }
        }

        public bool Z
        {
            get { return z; }
            set { z = value; }
        }

        public string path
        {
            get { return pathOfTail; }
        }

        public int width
        {
            get { return bmp.Width; }
        }

        public int height
        {
            get { return bmp.Height; }
        }

    }
}
