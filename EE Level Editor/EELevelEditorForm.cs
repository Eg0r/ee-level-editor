﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace EE_Level_Editor
{
    public partial class EELevelEditorForm : Form
    {
        private LvlData lvlData;
        private List<Tail> tails = new List<Tail>();

        private Bitmap img;
        private Graphics g;
        private Graphics gInstrument;

        private List<int> eventBmps = new List<int>();
        private bool paste = false;
        private bool dragDrop = false;
        private bool selection = false;
        private List<Point> deltaXY = new List<Point>();
        private Point moveXY;
        private Point selectXY;
        private Rectangle selectRect;

        private bool isEditing = false;

        int typeInstrument = 2;
        float scale = 1;

        private const int PRILIP = 8;
        private const int ADD_CANVAS_PX = 20;


        public EELevelEditorForm()
        {
            InitializeComponent();
            lvlCanvas.Enabled = false;
            lvlData = new LvlData();
            eventBmps.Add(-1);
            defaultArrowButton.PerformClick();
            openPicDialog.FileName = "";
            openPicDialog.Filter = "PNG Files (*.png)|*.png|JPEG Files (*.jpeg)|*.jpeg|JPG Files (*.jpg)|*.jpg|GIF Files (*.gif)|*.gif";
        }

        private void load()
        {
            lvlCanvas.Enabled = true;
            widthTextBox.Enabled = true;
            heightTextBox.Enabled = true;

            img = new Bitmap(lvlData.width, lvlData.height);
            lvlCanvas.Image = img;
            g = Graphics.FromImage(img);
            lvlCanvas.Width = lvlData.width;
            lvlCanvas.Height = lvlData.height;
            lvlCanvas.BackColor = Color.FromArgb(lvlData.color);

            gInstrument = Graphics.FromHwnd(lvlCanvas.Handle);

            this.widthTextBox.Text = lvlData.width.ToString();
            this.heightTextBox.Text = lvlData.height.ToString();
        }

        public void create(string name, int width, int height, Color color)
        {
            lvlData.setDescription(name, width, height, color.ToArgb());
            load();
        }

        private void redraw()
        {
            g.Clear(lvlCanvas.BackColor);
            for (int i = 0; i < tails.Count; i++)
            {
                g.DrawImageUnscaledAndClipped(tails[i].picture, new Rectangle(tails[i].X, tails[i].Y, tails[i].width, tails[i].height));
            }
        }

        private void создатьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CreateLevel crtLvl = new CreateLevel();
            crtLvl.ShowDialog(this);
        }

        private void addTail_Click(object sender, EventArgs e)
        {
            if (lvlCanvas.Enabled == true)
            {
                if (openPicDialog.ShowDialog().ToString() == "OK")
                {
                    defaultArrowButton.PerformClick();

                    paste = true;
                    lvlCanvas.Cursor = Cursors.UpArrow;

                    isEditing = true;
                }
            } 
            else
            {
                MessageBox.Show("Создайте уровень", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void открытьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog().ToString() == "OK")
            {
                StreamReader sr = new StreamReader(openFileDialog1.FileName);
                lvlData.loadDataFromJson(sr.ReadToEnd());
                sr.Close();
                load();
                tails = lvlData.toTails(Path.GetDirectoryName(openFileDialog1.FileName));
                int counts = 0;
                for (int i = 0; i < tails.Count; i++)
                {
                    if (tails[i].Z)
                    {
                        Console.WriteLine(i);
                        Tail tmp = tails[i];
                        tails.RemoveAt(i);
                        tails.Add(tmp);
                        counts++;
                    }
                }
                isEditing = false;
                redraw();
                lvlCanvas.Refresh();
            }
        }

        private void сохранитьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            saveFileDialog1.FileName = lvlData.name + ".txt";
            if (saveFileDialog1.ShowDialog().ToString() == "OK")
            {
                lvlData.convertTailsToBlockData(tails);
                StreamWriter sw = new StreamWriter(saveFileDialog1.FileName);
                sw.WriteLine(lvlData.toJson());
                sw.Close();
                isEditing = false;
            }
        }

        private void lvlCanvas_MouseUp(object sender, MouseEventArgs e)
        {
            dragDrop = false;
            if (selection)
            {
                eventBmps.RemoveRange(0, eventBmps.Count);
                redraw();
                int count = 0;
                for (int i = 0; i < tails.Count; i++)
                {
                    if (selectRect.Contains(new Rectangle(tails[i].X, tails[i].Y, tails[i].width, tails[i].height)))
                    {
                        eventBmps.Add(i);
                        count++;
                        g.DrawRectangle(new Pen(Color.Red, 3), new Rectangle(tails[i].X, tails[i].Y, tails[i].width, tails[i].height));
                    }
                }
                if (count > 1)
                {
                    contextMenuStrip1.Items[1].Enabled = false;
                }
                lvlCanvas.Refresh();
                selection = false;
            }
            selectRect = new Rectangle(0, 0, 0, 0);
            lvlCanvas.ContextMenuStrip = null;
        }

        private int convertWithScale(int x)
        {
            return (int)(x*(Math.Pow(scale, -1)));
        }

        private void newSizeLvl(int w, int h)
        {
            lvlData.setWidth(w);
            lvlData.setHeight(h);
            scaleLvl(scale, scale);
        }

        private void scaleLvl(float sw, float sh)
        {
            int w = (int)(lvlData.width * sw);
            int h = (int)(lvlData.height * sh);
            img = new Bitmap(img, w, h);
            lvlCanvas.Image = img;
            g = Graphics.FromImage(img);
            g.ScaleTransform(scale, scale);
            lvlCanvas.Width = w;
            lvlCanvas.Height = h;
            redraw();
            lvlCanvas.Refresh();
        }

        private void lvlCanvas_MouseMove(object sender, MouseEventArgs e)
        {
            coord.Text = convertWithScale(e.X).ToString() + "x" + convertWithScale(e.Y).ToString();

            if (dragDrop)
            {
                isEditing = true;
                if ((Math.Abs(moveXY.X - convertWithScale(e.X)) >= 5) || (Math.Abs(moveXY.Y - convertWithScale(e.Y)) >= 5))
                {
                    for (int iT = 0; iT < eventBmps.Count; iT++)
                    {
                        moveXY.X = convertWithScale(e.X);
                        moveXY.Y = convertWithScale(e.Y);
                        if ((convertWithScale(e.X) + deltaXY[iT].X >= 0) && (convertWithScale(e.X) + deltaXY[iT].X + tails[eventBmps[iT]].width <= convertWithScale(lvlCanvas.Width)))
                            tails[eventBmps[iT]].X = convertWithScale(e.X) + deltaXY[iT].X;
                        if ((convertWithScale(e.Y) + deltaXY[iT].Y >= 0) && (convertWithScale(e.Y) + deltaXY[iT].Y + tails[eventBmps[iT]].height <= convertWithScale(lvlCanvas.Height)))
                            tails[eventBmps[iT]].Y = convertWithScale(e.Y) + deltaXY[iT].Y;

                        for (int i = 0; i < tails.Count; i++)
                        {
                            if (i != eventBmps[iT])
                            {
                                if (Math.Abs(tails[eventBmps[iT]].X - tails[i].X - tails[i].picture.Width) <= PRILIP)
                                {
                                    tails[eventBmps[iT]].X = tails[i].X + tails[i].width;
                                }
                                if (Math.Abs(tails[eventBmps[iT]].X + tails[eventBmps[iT]].width - tails[i].X) <= PRILIP)
                                {
                                    tails[eventBmps[iT]].X = tails[i].X - tails[eventBmps[0]].width;
                                }
                                if (Math.Abs(tails[eventBmps[iT]].X - tails[i].X) <= PRILIP)
                                {
                                    tails[eventBmps[iT]].X = tails[i].X;
                                }
                                if (Math.Abs(tails[eventBmps[iT]].X + tails[eventBmps[iT]].width - tails[i].X - tails[i].width) <= PRILIP)
                                {
                                    tails[eventBmps[iT]].X = tails[i].X - tails[eventBmps[iT]].width + tails[i].width;
                                }


                                if (Math.Abs(tails[eventBmps[iT]].Y - tails[i].Y - tails[i].height) <= PRILIP)
                                {
                                    tails[eventBmps[iT]].Y = tails[i].Y + tails[i].height;
                                }
                                if (Math.Abs(tails[eventBmps[iT]].Y + tails[eventBmps[iT]].height - tails[i].Y) <= PRILIP)
                                {
                                    tails[eventBmps[iT]].Y = tails[i].Y - tails[eventBmps[iT]].height;
                                }
                                if (Math.Abs(tails[eventBmps[iT]].Y - tails[i].Y) <= PRILIP)
                                {
                                    tails[eventBmps[iT]].Y = tails[i].Y;
                                }
                                if (Math.Abs(tails[eventBmps[iT]].Y + tails[eventBmps[iT]].height - tails[i].Y - tails[i].height) <= PRILIP)
                                {
                                    tails[eventBmps[iT]].Y = tails[i].Y - tails[eventBmps[iT]].height + tails[i].height;
                                }
                            }
                        }
                    }
                    redraw();
                    for (int iT = 0; iT < eventBmps.Count; iT++)
                    {
                        g.DrawRectangle(new Pen(Color.Red, 3), new Rectangle(tails[eventBmps[iT]].X, tails[eventBmps[iT]].Y, tails[eventBmps[iT]].picture.Width, tails[eventBmps[iT]].picture.Height));
                    }
                    lvlCanvas.Refresh();
                }
            }

            if (selection)
            {
                if ((e.X >= 0) && (e.X <= lvlCanvas.Width) && (e.Y >= 0) && (e.Y <= lvlCanvas.Height))
                {
                    Pen p = new Pen(Brushes.Black, 3);
                    p.DashStyle = DashStyle.Dash;
                    lvlCanvas.Refresh();
                    int x = 1, y = 1, w = 1, h = 1;
                    if ((e.X < selectXY.X) && (e.Y < selectXY.Y))
                    {
                        x = e.X;
                        y = e.Y;
                        w = selectXY.X - e.X;
                        h = selectXY.Y - e.Y;
                    }
                    if ((e.X < selectXY.X) && (e.Y >= selectXY.Y))
                    {
                        x = e.X;
                        y = selectXY.Y;
                        w = selectXY.X - e.X;
                        h = e.Y - selectXY.Y;
                    }
                    if ((e.X >= selectXY.X) && (e.Y < selectXY.Y))
                    {
                        x = selectXY.X;
                        y = e.Y;
                        w = e.X - selectXY.X;
                        h = selectXY.Y - e.Y;
                    }
                    if ((e.X >= selectXY.X) && (e.Y >= selectXY.Y))
                    {
                        x = selectXY.X;
                        y = selectXY.Y;
                        w = e.X - selectXY.X;
                        h = e.Y - selectXY.Y;
                    }
                    selectRect = new Rectangle(convertWithScale(x), convertWithScale(y), convertWithScale(w), convertWithScale(h));
                    gInstrument.DrawRectangle(p, x, y, w, h);
                }
            }
        }

        private void lvlCanvas_MouseDown(object sender, MouseEventArgs e)
        {
            switch (typeInstrument) 
            {
                case 0: scale *= 2;
                    scaleLvl(scale, scale);
                    scaleLabel.Text = "x" + scale.ToString();
                    break;
                case 1: scale /= 2;
                    scaleLvl(scale, scale);
                    scaleLabel.Text = "x" + scale.ToString();
                    break;
                case 2:
                        if (paste == false)
                        {
                            if ((eventBmps.Count >= 0) && (eventBmps.Count <= 1))
                            {
                                for (int i = tails.Count - 1; i >= 0; i--)
                                {
                                    if ((tails[i].X <= convertWithScale(e.X)) && (tails[i].Y <= convertWithScale(e.Y)) && ((tails[i].X + tails[i].picture.Width) >= convertWithScale(e.X)) && (tails[i].Y + tails[i].picture.Height >= convertWithScale(e.Y)))
                                    {
                                        redraw();
                                        eventBmps.RemoveRange(0, eventBmps.Count);
                                        eventBmps.Add(i);
                                        contextMenuStrip1.Items[1].Enabled = true;
                                        dragDrop = true;
                                        selection = false;
                                        deltaXY.RemoveRange(0, deltaXY.Count);
                                        deltaXY.Add(new Point(tails[i].X - convertWithScale(e.X), tails[i].Y - convertWithScale(e.Y)));
                                        moveXY.X = convertWithScale(e.X);
                                        moveXY.Y = convertWithScale(e.Y);
                                        nameTailTextBox.Text = Path.GetFileNameWithoutExtension(tails[i].path);
                                        pathTailTextBox.Text = tails[i].path;
                                        lvlCanvas.ContextMenuStrip = contextMenuStrip1;
                                        g.DrawRectangle(new Pen(Color.Red, 3), new Rectangle(tails[i].X, tails[i].Y, tails[i].picture.Width, tails[i].picture.Height));
                                        break;
                                    }
                                }
                            }
                            else
                            {
                                deltaXY.RemoveRange(0, deltaXY.Count);
                                for (int i = tails.Count - 1; i >= 0; i--)
                                {
                                    if ((tails[i].X <= convertWithScale(e.X)) && (tails[i].Y <= convertWithScale(e.Y)) && ((tails[i].X + tails[i].picture.Width) >= convertWithScale(e.X)) && (tails[i].Y + tails[i].picture.Height >= convertWithScale(e.Y)))
                                    {
                                        if (eventBmps.Contains(i))
                                        {
                                            dragDrop = true;
                                            selection = false;
                                            moveXY.X = convertWithScale(e.X);
                                            moveXY.Y = convertWithScale(e.Y);
                                            nameTailTextBox.Text = "";
                                            pathTailTextBox.Text = "";
                                            lvlCanvas.ContextMenuStrip = contextMenuStrip1;
                                            for (int addingDelta = 0; addingDelta < eventBmps.Count; addingDelta++)
                                            {
                                                deltaXY.Add(new Point(tails[eventBmps[addingDelta]].X - convertWithScale(e.X), tails[eventBmps[addingDelta]].Y - convertWithScale(e.Y)));
                                            }
                                        }
                                    }
                                }
                            }
                            if (!dragDrop)
                            {
                                selection = true;
                                selectXY.X = e.X;
                                selectXY.Y = e.Y;
                            }
                        }
                        else
                        {
                            tails.Add(new Tail(openPicDialog.FileName, convertWithScale(e.X), convertWithScale(e.Y)));
                            lvlData.addBlockType(openPicDialog.FileName);
                            g.DrawImageUnscaledAndClipped(tails[tails.Count - 1].picture, new Rectangle(convertWithScale(e.X), convertWithScale(e.Y), tails[tails.Count - 1].picture.Width, tails[tails.Count - 1].picture.Height));
                            paste = false;
                            lvlCanvas.Cursor = Cursors.Default;
                    }
                    break;
            }
            lvlCanvas.Refresh();
        }

        private void textBoxDigital_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar <= 47 || e.KeyChar >= 58) && e.KeyChar != 8)
                e.Handled = true;
        }

        private void upWidth_Click(object sender, EventArgs e)
        {
            if (lvlCanvas.Enabled)
            {
                lvlData.addWidth(ADD_CANVAS_PX);
                widthTextBox.Text = (int.Parse(widthTextBox.Text) + ADD_CANVAS_PX).ToString();
                isEditing = true;
            }
            else
            {
                MessageBox.Show("Создайте уровень", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void downWidth_Click(object sender, EventArgs e)
        {
            if (lvlCanvas.Enabled)
            {
                if (lvlCanvas.Width > ADD_CANVAS_PX)
                {
                    lvlData.addWidth(-1 * ADD_CANVAS_PX);
                    widthTextBox.Text = (int.Parse(widthTextBox.Text) - 1 * ADD_CANVAS_PX).ToString();
                    isEditing = true;
                }
            }
            else
            {
                MessageBox.Show("Создайте уровень", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void upHeight_Click(object sender, EventArgs e)
        {
            if (lvlCanvas.Enabled)
            {
                lvlData.addHeight(ADD_CANVAS_PX);
                heightTextBox.Text = (int.Parse(heightTextBox.Text) + ADD_CANVAS_PX).ToString();
                isEditing = true;
            }
            else
            {
                MessageBox.Show("Создайте уровень", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void downHeight_Click(object sender, EventArgs e)
        {
            if (lvlCanvas.Enabled)
            {
                if (lvlCanvas.Height > ADD_CANVAS_PX)
                {
                    lvlData.addHeight(-1 * ADD_CANVAS_PX);
                    heightTextBox.Text = (int.Parse(heightTextBox.Text) - 1 * ADD_CANVAS_PX).ToString();
                    isEditing = true;
                }
            }
            else
            {
                MessageBox.Show("Создайте уровень", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void widthTextBox_TextChanged(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty((sender as TextBox).Text))
                (sender as TextBox).Text = "1";
            newSizeLvl(int.Parse(widthTextBox.Text), lvlData.height);
        }

        private void heightTextBox_TextChanged(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty((sender as TextBox).Text))
                (sender as TextBox).Text = "1";
            newSizeLvl(lvlData.width, int.Parse(heightTextBox.Text));
        }

        private void удалитьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int countRemove = -1;
            for (int i = 0; i < eventBmps.Count; i++)
            {
                countRemove++;
                tails.RemoveAt(eventBmps[i] - countRemove);
            }
            isEditing = true;
            redraw();
            lvlCanvas.Refresh();
        }

        private void zoomInButton_Click(object sender, EventArgs e)
        {
            typeInstrument = 0;
            zoomInButton.Checked = true;
            zoomOutButton.Checked = false;
            defaultArrowButton.Checked = false;
            lvlCanvas.Cursor = new Cursor(Properties.Resources.magnifier_zoom_in_ico.Handle);
        }

        private void zoomOutButton_Click(object sender, EventArgs e)
        {
            typeInstrument = 1;
            zoomInButton.Checked = false;
            zoomOutButton.Checked = true;
            defaultArrowButton.Checked = false;
            lvlCanvas.Cursor = new Cursor(Properties.Resources.magnifier_zoom_out_ico.Handle);
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            zoomOutButton.Checked = false;
            zoomInButton.Checked = false;
            defaultArrowButton.Checked = true;
            lvlCanvas.Cursor = Cursors.Default;
            typeInstrument = 2;
        }

        private void оПрограммеToolStripMenuItem_Click(object sender, EventArgs e)
        {
            About ab = new About();
            ab.ShowDialog(this);
        }

        private void влевоToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DigitalInputForm dif = new DigitalInputForm("Вытянуть блок влево");
            if (dif.ShowDialog(this) == DialogResult.OK)
            {
                int x = tails[eventBmps[0]].X;
                for (int  i = 0; i < dif.digitalValue; i++)
                {
                    x -= tails[eventBmps[0]].width;
                    if (x < 0)
                    {
                        MessageBox.Show("Невозможно вытянуть блоки, новые координаты которых превышают допустимые", "Уведомление", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        break;
                    }
                    tails.Add(new Tail(tails[eventBmps[0]].path, x, tails[eventBmps[0]].Y));
                }
                isEditing = true;
                redraw();
                lvlCanvas.Refresh();
            }
        }

        private void вправоToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DigitalInputForm dif = new DigitalInputForm("Вытянуть блок вправо");
            if (dif.ShowDialog(this) == DialogResult.OK)
            {
                int x = tails[eventBmps[0]].X;
                for (int i = 0; i < dif.digitalValue; i++)
                {
                    x += tails[eventBmps[0]].width;
                    if (x + tails[eventBmps[0]].width > lvlData.width)
                    {
                        MessageBox.Show("Невозможно вытянуть блоки, новые координаты которых превышают допустимые", "Уведомление", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        break;
                    }
                    tails.Add(new Tail(tails[eventBmps[0]].path, x, tails[eventBmps[0]].Y));
                }
                isEditing = true;
                redraw();
                lvlCanvas.Refresh();
            }
        }

        private void вверхToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DigitalInputForm dif = new DigitalInputForm("Вытянуть блок вверх");
            if (dif.ShowDialog(this) == DialogResult.OK)
            {
                int y = tails[eventBmps[0]].Y;
                for (int i = 0; i < dif.digitalValue; i++)
                {
                    y -= tails[eventBmps[0]].height;
                    if (y < 0)
                    {
                        MessageBox.Show("Невозможно вытянуть блоки, новые координаты которых превышают допустимые", "Уведомление", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        break;
                    }
                    tails.Add(new Tail(tails[eventBmps[0]].path, tails[eventBmps[0]].X, y));
                }
                isEditing = true;
                redraw();
                lvlCanvas.Refresh();
            }
        }

        private void внизToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DigitalInputForm dif = new DigitalInputForm("Вытянуть блок вниз");
            if (dif.ShowDialog(this) == DialogResult.OK)
            {
                int y = tails[eventBmps[0]].Y;
                for (int i = 0; i < dif.digitalValue; i++)
                {
                    y += tails[eventBmps[0]].height;
                    if (y + tails[eventBmps[0]].height > lvlData.height)
                    {
                        MessageBox.Show("Невозможно вытянуть блоки, новые координаты которых превышают допустимые", "Уведомление", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        break;
                    }
                    tails.Add(new Tail(tails[eventBmps[0]].path, tails[eventBmps[0]].X, y));
                }
                isEditing = true;
                redraw();
                lvlCanvas.Refresh();
            }
        }

        private void EELevelEditorForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (isEditing)
            {
                DialogResult result = MessageBox.Show("Сохранить изменения?", "Уведомление", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
                if (result == DialogResult.Yes)
                {
                    сохранитьToolStripMenuItem_Click(this, new EventArgs());
                }
                if (result == DialogResult.Cancel)
                {
                    e.Cancel = true;
                }
            }
        }

        private void наПереднийПланToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int countRemove = -1;
            for (int i = 0; i < eventBmps.Count; i++)
            {
                countRemove++;
                Tail tmp = tails[eventBmps[i] - countRemove];
                tmp.Z = true;
                tails.RemoveAt(eventBmps[i] - countRemove);
                tails.Add(tmp);
            }
            isEditing = true;
            redraw();
            lvlCanvas.Refresh();
        }
    }
}
