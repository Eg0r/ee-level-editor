﻿namespace EE_Level_Editor
{
    partial class EELevelEditorForm
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.файлToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.создатьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.открытьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.сохранитьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.выходToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.оПрограммеToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.scaleLabel = new System.Windows.Forms.Label();
            this.coord = new System.Windows.Forms.Label();
            this.downHeight = new System.Windows.Forms.Button();
            this.downWidth = new System.Windows.Forms.Button();
            this.upHeight = new System.Windows.Forms.Button();
            this.upWidth = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.heightTextBox = new System.Windows.Forms.TextBox();
            this.widthTextBox = new System.Windows.Forms.TextBox();
            this.button2 = new System.Windows.Forms.Button();
            this.addTail = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lvlCanvas = new System.Windows.Forms.PictureBox();
            this.openPicDialog = new System.Windows.Forms.OpenFileDialog();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.panel2 = new System.Windows.Forms.Panel();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.pathTailTextBox = new System.Windows.Forms.TextBox();
            this.nameTailTextBox = new System.Windows.Forms.TextBox();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.удалитьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.вытянутьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.влевоToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.вправоToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.вверхToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.внизToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.zoomInButton = new System.Windows.Forms.ToolStripButton();
            this.zoomOutButton = new System.Windows.Forms.ToolStripButton();
            this.defaultArrowButton = new System.Windows.Forms.ToolStripButton();
            this.наПереднийПланToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lvlCanvas)).BeginInit();
            this.panel2.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.contextMenuStrip1.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.файлToolStripMenuItem,
            this.оПрограммеToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(822, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // файлToolStripMenuItem
            // 
            this.файлToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.создатьToolStripMenuItem,
            this.открытьToolStripMenuItem,
            this.сохранитьToolStripMenuItem,
            this.выходToolStripMenuItem});
            this.файлToolStripMenuItem.Name = "файлToolStripMenuItem";
            this.файлToolStripMenuItem.Size = new System.Drawing.Size(48, 20);
            this.файлToolStripMenuItem.Text = "Файл";
            // 
            // создатьToolStripMenuItem
            // 
            this.создатьToolStripMenuItem.Name = "создатьToolStripMenuItem";
            this.создатьToolStripMenuItem.Size = new System.Drawing.Size(132, 22);
            this.создатьToolStripMenuItem.Text = "Создать";
            this.создатьToolStripMenuItem.Click += new System.EventHandler(this.создатьToolStripMenuItem_Click);
            // 
            // открытьToolStripMenuItem
            // 
            this.открытьToolStripMenuItem.Name = "открытьToolStripMenuItem";
            this.открытьToolStripMenuItem.Size = new System.Drawing.Size(132, 22);
            this.открытьToolStripMenuItem.Text = "Открыть";
            this.открытьToolStripMenuItem.Click += new System.EventHandler(this.открытьToolStripMenuItem_Click);
            // 
            // сохранитьToolStripMenuItem
            // 
            this.сохранитьToolStripMenuItem.Name = "сохранитьToolStripMenuItem";
            this.сохранитьToolStripMenuItem.Size = new System.Drawing.Size(132, 22);
            this.сохранитьToolStripMenuItem.Text = "Сохранить";
            this.сохранитьToolStripMenuItem.Click += new System.EventHandler(this.сохранитьToolStripMenuItem_Click);
            // 
            // выходToolStripMenuItem
            // 
            this.выходToolStripMenuItem.Name = "выходToolStripMenuItem";
            this.выходToolStripMenuItem.Size = new System.Drawing.Size(132, 22);
            this.выходToolStripMenuItem.Text = "Выход";
            // 
            // оПрограммеToolStripMenuItem
            // 
            this.оПрограммеToolStripMenuItem.Name = "оПрограммеToolStripMenuItem";
            this.оПрограммеToolStripMenuItem.Size = new System.Drawing.Size(94, 20);
            this.оПрограммеToolStripMenuItem.Text = "О программе";
            this.оПрограммеToolStripMenuItem.Click += new System.EventHandler(this.оПрограммеToolStripMenuItem_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.scaleLabel);
            this.groupBox1.Controls.Add(this.coord);
            this.groupBox1.Controls.Add(this.downHeight);
            this.groupBox1.Controls.Add(this.downWidth);
            this.groupBox1.Controls.Add(this.upHeight);
            this.groupBox1.Controls.Add(this.upWidth);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.heightTextBox);
            this.groupBox1.Controls.Add(this.widthTextBox);
            this.groupBox1.Controls.Add(this.button2);
            this.groupBox1.Controls.Add(this.addTail);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(137, 204);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Панель инструментов";
            // 
            // scaleLabel
            // 
            this.scaleLabel.AutoSize = true;
            this.scaleLabel.Location = new System.Drawing.Point(80, 179);
            this.scaleLabel.Name = "scaleLabel";
            this.scaleLabel.Size = new System.Drawing.Size(18, 13);
            this.scaleLabel.TabIndex = 10;
            this.scaleLabel.Text = "x1";
            // 
            // coord
            // 
            this.coord.AutoSize = true;
            this.coord.Location = new System.Drawing.Point(26, 179);
            this.coord.Name = "coord";
            this.coord.Size = new System.Drawing.Size(24, 13);
            this.coord.TabIndex = 9;
            this.coord.Text = "0x0";
            // 
            // downHeight
            // 
            this.downHeight.Location = new System.Drawing.Point(80, 152);
            this.downHeight.Name = "downHeight";
            this.downHeight.Size = new System.Drawing.Size(34, 14);
            this.downHeight.TabIndex = 8;
            this.downHeight.Text = "ˇ";
            this.downHeight.UseVisualStyleBackColor = true;
            this.downHeight.Click += new System.EventHandler(this.downHeight_Click);
            // 
            // downWidth
            // 
            this.downWidth.Location = new System.Drawing.Point(22, 152);
            this.downWidth.Name = "downWidth";
            this.downWidth.Size = new System.Drawing.Size(34, 14);
            this.downWidth.TabIndex = 7;
            this.downWidth.Text = "ˇ";
            this.downWidth.UseVisualStyleBackColor = true;
            this.downWidth.Click += new System.EventHandler(this.downWidth_Click);
            // 
            // upHeight
            // 
            this.upHeight.Location = new System.Drawing.Point(80, 106);
            this.upHeight.Name = "upHeight";
            this.upHeight.Size = new System.Drawing.Size(34, 14);
            this.upHeight.TabIndex = 6;
            this.upHeight.Text = "^";
            this.upHeight.UseVisualStyleBackColor = true;
            this.upHeight.Click += new System.EventHandler(this.upHeight_Click);
            // 
            // upWidth
            // 
            this.upWidth.Location = new System.Drawing.Point(22, 106);
            this.upWidth.Name = "upWidth";
            this.upWidth.Size = new System.Drawing.Size(34, 14);
            this.upWidth.TabIndex = 5;
            this.upWidth.Text = "^";
            this.upWidth.UseVisualStyleBackColor = true;
            this.upWidth.Click += new System.EventHandler(this.upWidth_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(62, 129);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(12, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "x";
            // 
            // heightTextBox
            // 
            this.heightTextBox.Enabled = false;
            this.heightTextBox.Location = new System.Drawing.Point(80, 126);
            this.heightTextBox.Name = "heightTextBox";
            this.heightTextBox.Size = new System.Drawing.Size(34, 20);
            this.heightTextBox.TabIndex = 3;
            this.heightTextBox.TextChanged += new System.EventHandler(this.heightTextBox_TextChanged);
            this.heightTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxDigital_KeyPress);
            // 
            // widthTextBox
            // 
            this.widthTextBox.Enabled = false;
            this.widthTextBox.Location = new System.Drawing.Point(22, 126);
            this.widthTextBox.Name = "widthTextBox";
            this.widthTextBox.Size = new System.Drawing.Size(34, 20);
            this.widthTextBox.TabIndex = 2;
            this.widthTextBox.TextChanged += new System.EventHandler(this.widthTextBox_TextChanged);
            this.widthTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxDigital_KeyPress);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(29, 64);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(80, 36);
            this.button2.TabIndex = 1;
            this.button2.Text = "Объединить тайлы";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // addTail
            // 
            this.addTail.Location = new System.Drawing.Point(29, 22);
            this.addTail.Name = "addTail";
            this.addTail.Size = new System.Drawing.Size(75, 36);
            this.addTail.TabIndex = 0;
            this.addTail.Text = "Добавить тайл";
            this.addTail.UseVisualStyleBackColor = true;
            this.addTail.Click += new System.EventHandler(this.addTail_Click);
            // 
            // panel1
            // 
            this.panel1.AutoScroll = true;
            this.panel1.AutoScrollMargin = new System.Drawing.Size(5, 5);
            this.panel1.AutoScrollMinSize = new System.Drawing.Size(10, 10);
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.lvlCanvas);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 49);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(683, 346);
            this.panel1.TabIndex = 3;
            // 
            // lvlCanvas
            // 
            this.lvlCanvas.Location = new System.Drawing.Point(0, 0);
            this.lvlCanvas.Name = "lvlCanvas";
            this.lvlCanvas.Size = new System.Drawing.Size(100, 50);
            this.lvlCanvas.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.lvlCanvas.TabIndex = 0;
            this.lvlCanvas.TabStop = false;
            this.lvlCanvas.MouseDown += new System.Windows.Forms.MouseEventHandler(this.lvlCanvas_MouseDown);
            this.lvlCanvas.MouseMove += new System.Windows.Forms.MouseEventHandler(this.lvlCanvas_MouseMove);
            this.lvlCanvas.MouseUp += new System.Windows.Forms.MouseEventHandler(this.lvlCanvas_MouseUp);
            // 
            // openPicDialog
            // 
            this.openPicDialog.FileName = "openFileDialog1";
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.groupBox1);
            this.panel2.Controls.Add(this.groupBox2);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel2.Location = new System.Drawing.Point(683, 24);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(139, 371);
            this.panel2.TabIndex = 1;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.pathTailTextBox);
            this.groupBox2.Controls.Add(this.nameTailTextBox);
            this.groupBox2.Location = new System.Drawing.Point(3, 210);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(136, 158);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Тайл инфо";
            // 
            // pathTailTextBox
            // 
            this.pathTailTextBox.Location = new System.Drawing.Point(19, 88);
            this.pathTailTextBox.Name = "pathTailTextBox";
            this.pathTailTextBox.ReadOnly = true;
            this.pathTailTextBox.Size = new System.Drawing.Size(100, 20);
            this.pathTailTextBox.TabIndex = 1;
            // 
            // nameTailTextBox
            // 
            this.nameTailTextBox.Location = new System.Drawing.Point(19, 35);
            this.nameTailTextBox.Name = "nameTailTextBox";
            this.nameTailTextBox.ReadOnly = true;
            this.nameTailTextBox.Size = new System.Drawing.Size(100, 20);
            this.nameTailTextBox.TabIndex = 0;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.удалитьToolStripMenuItem,
            this.вытянутьToolStripMenuItem,
            this.наПереднийПланToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(176, 92);
            // 
            // удалитьToolStripMenuItem
            // 
            this.удалитьToolStripMenuItem.Name = "удалитьToolStripMenuItem";
            this.удалитьToolStripMenuItem.Size = new System.Drawing.Size(175, 22);
            this.удалитьToolStripMenuItem.Text = "Удалить";
            this.удалитьToolStripMenuItem.Click += new System.EventHandler(this.удалитьToolStripMenuItem_Click);
            // 
            // вытянутьToolStripMenuItem
            // 
            this.вытянутьToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.влевоToolStripMenuItem,
            this.вправоToolStripMenuItem,
            this.вверхToolStripMenuItem,
            this.внизToolStripMenuItem});
            this.вытянутьToolStripMenuItem.Name = "вытянутьToolStripMenuItem";
            this.вытянутьToolStripMenuItem.Size = new System.Drawing.Size(175, 22);
            this.вытянутьToolStripMenuItem.Text = "Вытянуть";
            // 
            // влевоToolStripMenuItem
            // 
            this.влевоToolStripMenuItem.Name = "влевоToolStripMenuItem";
            this.влевоToolStripMenuItem.Size = new System.Drawing.Size(114, 22);
            this.влевоToolStripMenuItem.Text = "Влево";
            this.влевоToolStripMenuItem.Click += new System.EventHandler(this.влевоToolStripMenuItem_Click);
            // 
            // вправоToolStripMenuItem
            // 
            this.вправоToolStripMenuItem.Name = "вправоToolStripMenuItem";
            this.вправоToolStripMenuItem.Size = new System.Drawing.Size(114, 22);
            this.вправоToolStripMenuItem.Text = "Вправо";
            this.вправоToolStripMenuItem.Click += new System.EventHandler(this.вправоToolStripMenuItem_Click);
            // 
            // вверхToolStripMenuItem
            // 
            this.вверхToolStripMenuItem.Name = "вверхToolStripMenuItem";
            this.вверхToolStripMenuItem.Size = new System.Drawing.Size(114, 22);
            this.вверхToolStripMenuItem.Text = "Вверх";
            this.вверхToolStripMenuItem.Click += new System.EventHandler(this.вверхToolStripMenuItem_Click);
            // 
            // внизToolStripMenuItem
            // 
            this.внизToolStripMenuItem.Name = "внизToolStripMenuItem";
            this.внизToolStripMenuItem.Size = new System.Drawing.Size(114, 22);
            this.внизToolStripMenuItem.Text = "Вниз";
            this.внизToolStripMenuItem.Click += new System.EventHandler(this.внизToolStripMenuItem_Click);
            // 
            // toolStrip1
            // 
            this.toolStrip1.BackColor = System.Drawing.SystemColors.Control;
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.zoomInButton,
            this.zoomOutButton,
            this.defaultArrowButton});
            this.toolStrip1.Location = new System.Drawing.Point(0, 24);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Padding = new System.Windows.Forms.Padding(0, 1, 1, 0);
            this.toolStrip1.Size = new System.Drawing.Size(683, 25);
            this.toolStrip1.TabIndex = 1;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // zoomInButton
            // 
            this.zoomInButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.zoomInButton.Image = global::EE_Level_Editor.Properties.Resources.magnifier_zoom_in1;
            this.zoomInButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.zoomInButton.Name = "zoomInButton";
            this.zoomInButton.Size = new System.Drawing.Size(23, 21);
            this.zoomInButton.Text = "toolStripButton1";
            this.zoomInButton.Click += new System.EventHandler(this.zoomInButton_Click);
            // 
            // zoomOutButton
            // 
            this.zoomOutButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.zoomOutButton.Image = global::EE_Level_Editor.Properties.Resources.magnifier_zoom_out1;
            this.zoomOutButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.zoomOutButton.Name = "zoomOutButton";
            this.zoomOutButton.Size = new System.Drawing.Size(23, 21);
            this.zoomOutButton.Text = "toolStripButton2";
            this.zoomOutButton.Click += new System.EventHandler(this.zoomOutButton_Click);
            // 
            // defaultArrowButton
            // 
            this.defaultArrowButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.defaultArrowButton.Image = global::EE_Level_Editor.Properties.Resources.Mouse_Pointer_icon;
            this.defaultArrowButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.defaultArrowButton.Name = "defaultArrowButton";
            this.defaultArrowButton.Size = new System.Drawing.Size(23, 21);
            this.defaultArrowButton.Text = "toolStripButton1";
            this.defaultArrowButton.Click += new System.EventHandler(this.toolStripButton1_Click);
            // 
            // наПереднийПланToolStripMenuItem
            // 
            this.наПереднийПланToolStripMenuItem.Name = "наПереднийПланToolStripMenuItem";
            this.наПереднийПланToolStripMenuItem.Size = new System.Drawing.Size(175, 22);
            this.наПереднийПланToolStripMenuItem.Text = "На передний план";
            this.наПереднийПланToolStripMenuItem.Click += new System.EventHandler(this.наПереднийПланToolStripMenuItem_Click);
            // 
            // EELevelEditorForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(822, 395);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "EELevelEditorForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "EE Level Editor";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.EELevelEditorForm_FormClosing);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lvlCanvas)).EndInit();
            this.panel2.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.contextMenuStrip1.ResumeLayout(false);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem файлToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem создатьToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem открытьToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem сохранитьToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem выходToolStripMenuItem;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button addTail;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox lvlCanvas;
        private System.Windows.Forms.OpenFileDialog openPicDialog;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox heightTextBox;
        private System.Windows.Forms.TextBox widthTextBox;
        private System.Windows.Forms.Button downHeight;
        private System.Windows.Forms.Button downWidth;
        private System.Windows.Forms.Button upHeight;
        private System.Windows.Forms.Button upWidth;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox pathTailTextBox;
        private System.Windows.Forms.TextBox nameTailTextBox;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem удалитьToolStripMenuItem;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton zoomInButton;
        private System.Windows.Forms.ToolStripButton zoomOutButton;
        private System.Windows.Forms.ToolStripMenuItem оПрограммеToolStripMenuItem;
        private System.Windows.Forms.ToolStripButton defaultArrowButton;
        private System.Windows.Forms.Label coord;
        private System.Windows.Forms.Label scaleLabel;
        private System.Windows.Forms.ToolStripMenuItem вытянутьToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem влевоToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem вправоToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem вверхToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem внизToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem наПереднийПланToolStripMenuItem;
    }
}

