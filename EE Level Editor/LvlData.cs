﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Drawing;

namespace EE_Level_Editor
{
    /*{
        "level": {
            "description": {
                "name": "hjhkhkj",
                "width": 400,
                "height": 500
            },
            "data": {
            }
        }
    }*/
    [DataContract]
    class description
    {
        [DataMember]
        internal string name;
        [DataMember]
        internal int width;
        [DataMember]
        internal int height;
        [DataMember]
        internal int color;
    }

    [DataContract]
    class blockinfo
    {
        [DataMember]
        internal string name;
        [DataMember]
        internal string path;

        public blockinfo(string n, string p)
        {
            name = n;
            path = p;
        }
    }

    [DataContract]
    class blockdata
    {
        [DataMember]
        internal List<constructiondata> cdata;
        [DataMember]
        internal List<horizontaldata> hdata;
        [DataMember]
        internal List<verticaldata> vdata;

        public blockdata()
        {
            cdata = new List<constructiondata>();
            hdata = new List<horizontaldata>();
            vdata = new List<verticaldata>();
        }
    }

    [DataContract]
    class constructiondata
    {
        [DataMember]
        internal string name;
        [DataMember]
        internal int x;
        [DataMember]
        internal int y;
        [DataMember]
        internal bool z;

        public constructiondata()
        {

        }

        public constructiondata(string n, int ix, int iy, bool iz)
        {
            name = n;
            x = ix;
            y = iy;
            z = iz;
        }
    }

    [DataContract]
    class horizontaldata : constructiondata
    {
        [DataMember]
        internal int count;

        public horizontaldata(string n, int ix, int iy, int c, bool iz)
        {
            name = n;
            x = ix;
            y = iy;
            count = c;
            z = iz;
        }
    }

    [DataContract]
    class verticaldata : constructiondata
    {
        [DataMember]
        internal int count;

        public verticaldata(string n, int ix, int iy, int c, bool iz)
        {
            name = n;
            x = ix;
            y = iy;
            count = c;
            z = iz;
        }
    }

    [DataContract]
    class level
    {
        [DataMember]
        internal description descript;
        [DataMember]
        internal List<blockinfo> blocki;
        [DataMember]
        internal blockdata bdata;
    }

    class LvlData
    {
        private level lvl; 

        public LvlData()
        {
            lvl = new level();
            lvl.descript = new description();
            lvl.blocki = new List<blockinfo>();
            lvl.bdata = new blockdata();
        }

        public void loadDataFromJson(string json)
        {
            MemoryStream stream = new MemoryStream(Encoding.UTF8.GetBytes(json));
            DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(level));
            lvl = (level)serializer.ReadObject(stream);
        }

        public string nameToPath(string name, string exceptionPath)
        {
            for (int i = 0; i < lvl.blocki.Count; i++)
            {
                if (lvl.blocki[i].path.Contains(name))
                {
                    if (File.Exists(lvl.blocki[i].path))
                    {
                        return lvl.blocki[i].path;
                    } 
                    else
                    {
                        return exceptionPath + "/" + Path.GetFileName(lvl.blocki[i].path);
                    }
                }
            }
            return "";
        }

        public List<Tail> toTails(string exceptionPath)
        {
            List<Tail> tails = new List<Tail>();
            for (int i = 0; i < lvl.bdata.cdata.Count; i++)
            {
                tails.Add(new Tail(nameToPath(lvl.bdata.cdata[i].name, exceptionPath), lvl.bdata.cdata[i].x, lvl.bdata.cdata[i].y, lvl.bdata.cdata[i].z));
            }
            for (int i = 0; i < lvl.bdata.hdata.Count; i++)
            {
                tails.Add(new Tail(nameToPath(lvl.bdata.hdata[i].name, exceptionPath), lvl.bdata.hdata[i].x, lvl.bdata.hdata[i].y));
                Bitmap bmp = new Bitmap(nameToPath(lvl.bdata.hdata[i].name, exceptionPath));
                int x = lvl.bdata.hdata[i].x + bmp.Width;
                for (int adding = 1; adding < lvl.bdata.hdata[i].count; adding++)
                {
                    tails.Add(new Tail(nameToPath(lvl.bdata.hdata[i].name, exceptionPath), x, lvl.bdata.hdata[i].y, lvl.bdata.hdata[i].z));
                    x += bmp.Width;
                }
            }
            for (int i = 0; i < lvl.bdata.vdata.Count; i++)
            {
                tails.Add(new Tail(nameToPath(lvl.bdata.vdata[i].name, exceptionPath), lvl.bdata.vdata[i].x, lvl.bdata.vdata[i].y));
                Bitmap bmp = new Bitmap(nameToPath(lvl.bdata.vdata[i].name, exceptionPath));
                int y = lvl.bdata.vdata[i].y + bmp.Height;
                for (int adding = 1; adding < lvl.bdata.vdata[i].count; adding++)
                {
                    tails.Add(new Tail(nameToPath(lvl.bdata.vdata[i].name, exceptionPath), lvl.bdata.vdata[i].x, y, lvl.bdata.vdata[i].z));
                    y += bmp.Height;
                }
            }
            return tails;
        }

        public string toJson()
        {
            Encoding encoding = Encoding.UTF8;
            var serializer = new DataContractJsonSerializer(typeof(level));
            var stream = new MemoryStream();
            var writer = JsonReaderWriterFactory.CreateJsonWriter(stream, encoding);
            serializer.WriteObject(stream, lvl);
            return encoding.GetString(stream.ToArray());
        }

        public string name
        {
            get { return lvl.descript.name; }
        }

        public int width
        {
            get { return lvl.descript.width; }
        }

        public int height
        {
            get { return lvl.descript.height; }
        }

        public int color
        {
            get { return lvl.descript.color; }
        }

        public void setDescription(string name, int width, int height, int color)
        {
            lvl.descript.name = name;
            lvl.descript.width = width;
            lvl.descript.height = height;
            lvl.descript.color = color;
        }

        public void addWidth(int x)
        {
            lvl.descript.width += x;
        }

        public void addHeight(int x)
        {
            lvl.descript.height += x;
        }

        public void addBlockType(string path)
        {
            bool isExist = false;
            for (int i = 0; i < lvl.blocki.Count; i++)
            {
                if (lvl.blocki[i].path.Equals(path))
                {
                    isExist = true;
                }
            }
            if (!isExist)
            {
                lvl.blocki.Add(new blockinfo(Path.GetFileNameWithoutExtension(path), path));
            }
        }

        public void setWidth(int width)
        {
            lvl.descript.width = width;
        }

        public void setHeight(int height)
        {
            lvl.descript.height = height;
        }

        public void convertTailsToBlockData(List<Tail> tails)
        {
            lvl.bdata = new blockdata();
            tails.Sort((a, b) =>
            {
                int result = a.path.CompareTo(b.path);
                if (result == 0)
                    result = a.X.CompareTo(b.X);
                if (result == 0)
                    result = a.Y.CompareTo(b.Y);
                return result;
            });

            for (int  i = 0; i < tails.Count; i++)
            {
                if ((i + 1 != tails.Count) && (tails[i].path.Equals(tails[i+1].path)))
                {
                    int hor = 0;
                    for (int k = i; k < tails.Count - 1; k++)
                    {
                        if (tails[k].X + tails[k].width == tails[k + 1].X)
                        {
                            hor++;
                        }
                        else
                        {
                            break;
                        }
                    }
                    if (hor == 0)
                    {
                        int ver = 0;
                        for (int k = i; k < tails.Count - 1; k++)
                        {
                            if (tails[k].Y + tails[k].height == tails[k + 1].Y)
                            {
                                ver++;
                            }
                            else
                            {
                                break;
                            }
                        }
                        if (ver != 0)
                        {
                            lvl.bdata.vdata.Add(new verticaldata(Path.GetFileNameWithoutExtension(tails[i].path), tails[i].X, tails[i].Y, ver + 1, tails[i].Z));
                            i += ver;
                            continue;
                        }
                    }
                    else
                    {
                        lvl.bdata.hdata.Add(new horizontaldata(Path.GetFileNameWithoutExtension(tails[i].path), tails[i].X, tails[i].Y, hor+1, tails[i].Z));
                        i += hor;
                        continue;
                    }
                }
                lvl.bdata.cdata.Add(new constructiondata(Path.GetFileNameWithoutExtension(tails[i].path), tails[i].X, tails[i].Y, tails[i].Z));
            }
        }
    }
}
